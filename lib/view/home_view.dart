import 'package:flutter/material.dart';
import 'package:flutter_sqlite_demo/helper/database_helper.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  TextEditingController txtName = TextEditingController();
  TextEditingController txtAge = TextEditingController();
  TextEditingController txtGender = TextEditingController();

  @override
  void initState() {
    // dropDb();
    // updateUserTable();
    // deleteUserTableRecord();
    getDataFromUserTable();
    // getDataFromUserTableById();
    getCountTableUser();
    super.initState();
  }

  dropDb() async {
    final result =
        await DatabaseHelper.instance.dropTable(DatabaseHelper.tableUser);
    print("data result=> $result");
  }

  Future<int> insertToUserTable({var name, var age, var gender}) async {
    Map<String, dynamic> row = {
      DatabaseHelper.columnName: '$name',
      DatabaseHelper.columnAge: age,
      DatabaseHelper.columnGender: "$gender"
    };
    int id =
        await DatabaseHelper.instance.insert(row, DatabaseHelper.tableUser);
    return id;
  }

  getDataFromUserTable() async {
    final result =
        await DatabaseHelper.instance.queryAllRows(DatabaseHelper.tableUser);
    result.forEach((element) {
      print("data => ${element.values}");
    });
  }

  getDataFromUserTableById(int id) async {
    final result = await DatabaseHelper.instance
        .queryRowById(DatabaseHelper.tableUser, id);
    print("result query=> ${result[0].values}");
  }

  getCountTableUser() async {
    final recordCount =
        await DatabaseHelper.instance.queryRowCount(DatabaseHelper.tableUser);
    print("recordCount=> $recordCount");
  }

  updateUserTable() async {
    Map<String, dynamic> row = {
      DatabaseHelper.columnName: 'Miya',
      DatabaseHelper.columnAge: 18,
      DatabaseHelper.columnGender: "Female"
    };
    final result = await DatabaseHelper.instance
        .update(row, DatabaseHelper.tableUser, "1");
    print("data $result");
  }

  deleteUserTableRecord() async {
    final result =
        await DatabaseHelper.instance.delete(13, DatabaseHelper.tableUser);
    print("result=> $result");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        child: Column(
          children: [
            TextFormField(
              controller: txtName,
              decoration: InputDecoration(hintText: "Input Name"),
            ),
            TextFormField(
              controller: txtAge,
              decoration: InputDecoration(hintText: "Input Age"),
            ),
            TextFormField(
              controller: txtGender,
              decoration: InputDecoration(hintText: "Input Gender"),
            ),
            ElevatedButton(
                onPressed: () async {
                  int id = await insertToUserTable(
                      name: txtName.text,
                      age: txtAge.text,
                      gender: txtGender.text);
                  getDataFromUserTableById(id);
                },
                child: Text("Save Data"))
          ],
        ),
      ),
    );
  }
}
